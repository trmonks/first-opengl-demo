#include <GLFW/glfw3.h>
#include <cmath>
#include <GL/glu.h>
#include <iostream>

void drawScene(GLFWwindow *window, double t);

struct Eye {
    // Position of Eye
    double x, y, z;

    // Direction eye is looking
    double theta, phi;
} e;



int main()
{
    e.x = e.y = e.phi = e.theta = 0.0;
    e.z = 10.0;
    double t, delta_t;
    t = delta_t = 0.0;

    bool running = true;

    // First we need to get a GL context
    glfwInit();

    // Now open a window
    GLFWwindow *window = glfwCreateWindow(640, 480, "OpenGLOL", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return 0;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);
    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

    // Main loop
    while (running) {

        // Get current time
        delta_t = t;
        t = glfwGetTime();
        delta_t = t - delta_t;

        // Call our rendering function
        drawScene(window, t);

        // Swap buffers
        glfwSwapBuffers(window);
        glfwPollEvents();

        // Exit if ESC was pressed or window was closed
        running = !glfwGetKey(window, GLFW_KEY_ESCAPE) && !glfwWindowShouldClose(window);

        if(glfwGetKey(window, GLFW_KEY_UP)) {
            e.z += 7*sin(e.theta) * delta_t;
            e.x += 7*cos(e.theta) * delta_t;
        }
        if(glfwGetKey(window, GLFW_KEY_DOWN)) {
            e.z -= 7*sin(e.theta) * delta_t;
            e.x -= 7*cos(e.theta) * delta_t;
        }
        if(glfwGetKey(window, GLFW_KEY_LEFT)) {
            e.theta -= delta_t;
            //e.x -= 0.2;
        }
        if(glfwGetKey(window, GLFW_KEY_RIGHT)) {
            e.theta += delta_t;
            //e.x += 0.2;
        }
    }

    // We are done, clean up and exit
    glfwTerminate();
    return 0;
}

void drawScene(GLFWwindow *window, double t)
{
    int width, height;

    // Get window size
    glfwGetFramebufferSize(window, &width, &height);

    // Set viewport
    glViewport(0, 0, width, height);

    // Clear color and depth buffers
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Set up the projection matrix
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    gluPerspective(                     // Set perspective view
        65.0,                           // Field of view = 65 degrees
        (double)width/(double)height,   // Window aspect
        1.0,                            // Near Z clipping plane
        100.0                           // Far Z clipping
    );

    // Enable Z buffering
    glEnable( GL_DEPTH_TEST );
    glDepthFunc( GL_LEQUAL );

    // Set up the modelview matrix
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(  // Set camera position and orientation
        e.x, e.y, e.z,
        e.x + cos(e.theta), 0.0, e.z + sin(e.theta),
        0.0, 1.0, 0.0
    );

    glPushMatrix();

    // Translate the shape to the upper left
    glTranslatef(-4.0, 3.0, 0.0);

    // Rotate
    glRotatef(35.0 * (float)t, 0.0, 0.0, 1.0);
    glRotatef(60.0 * (float)t, 1.0, 0.0, 0.0);

    // Draw the shape
    glBegin(GL_POINTS); {
        for (int i = 0; i < 20; ++i) {
            glColor3f(1.0, 0.05 * (float)i, 0.0);
            glVertex3f(2.0 * (float)cos(0.31416 * (double)i),
                2.0 * (float)sin(0.31416 * (double)i),
                0.0);
        }
    }
    glEnd();

    // We are done with this transform
    glPopMatrix();

    glPushMatrix();

    // Translate the shape to the upper right
    glTranslatef(4.0, 3.0, 0.0);

    // Rotate the points about the z-axis and the x-axis
    glRotatef( 45.0f * (float)t, 0.0f, 0.0f, 1.0f );
    glRotatef( 55.0f * (float)t, 1.0f, 0.0f, 0.0f );

    glBegin(GL_LINE_LOOP);
    for (int i = 0; i < 20; ++i) {
        glColor3f(1.0, 0.05 * (float)i, 0.0);
        glVertex3f(2.0 * (float)cos(0.31416 * (double)i),
                   2.0 * (float)sin(0.31416 * (double)i),
                   0.0);
    }
    glEnd();

    glPopMatrix();

    glPushMatrix();

    // Translate the triangles to the lower left of the display
    glTranslatef(-4.0, -3.0, 0.0);

    // Rotate the triangles
    glRotatef(25.0 * (float)t, 0.0, 0.0, 1.0);
    glRotatef(75.0 * (float)t, 1.0, 0.0, 0.0);

    glBegin(GL_TRIANGLE_FAN); {
        glColor3f(0.0, 0.5, 1.0);
        glVertex3f(0.0, 0.0, 0.0);

        for (int i = 0; i < 21; ++i) {
            glColor3f(0.0, 0.5 * (float)i, 1.0);
            glVertex3f(2.0f * (float)cos(0.31416 * (double)i),
            2.0f * (float)sin(0.31416 * (double)i),
            0.0f);
        }
    }
    glEnd();
    glPopMatrix();

    glPushMatrix();

    // Translate to the lower right
    glTranslatef(4.0, -3.0, 0.0);

    // Rotate
    glRotatef(65.0 * (float)t, 0.0, 0.0, 1.0);
    glRotatef(-35.0 * (float)t, 1.0, 0.0, 0.0);

    // Draw the quad
    glBegin(GL_POLYGON); {
        for (int i = 0; i < 20; ++i) {
            glColor3f(1.0, 0.0, 0.5 * (float)i);
            glVertex3f(2.0 * (float)cos(0.31416 * (double)i),
                       2.0 * (float)sin(0.31416 * (double)i),
                       0.0);
        }
    }
    glEnd();

    glPopMatrix();

    // Save the current modelview matrix on the stack
    glPushMatrix();

    // Rotate the quad about the y-axis
    glRotatef( 60.0f * (float)t, 0.0f, 1.0f, 0.0f );

    // Now draw the quad
    glBegin( GL_QUADS ); {
        glColor3f(1.0, 0.0, 0.0);
        glVertex3f(-1.5, -1.5, 0.0);
        glColor3f(1.0, 1.0, 0.0);
        glVertex3f(1.5, -1.5, 0.0);
        glColor3f(1.0, 0.0, 1.0);
        glVertex3f(1.5,  1.5, 0.0);
        glColor3f(0.0, 0.0, 1.0);
        glVertex3f(-1.5, 1.5, 0.0);
    }
    glEnd();

    // Restore modelview matrix
    glPopMatrix();

    // Here we shall draw the floor
    glBegin(GL_POLYGON); {
        glColor3f(0.5, 0.5, 0.5);
        glVertex3f(-10.0, -7.0, -10.0);
        glVertex3f(-10.0, -7.0, 10.0);
        glVertex3f(10.0, -7.0, 10.0);
        glVertex3f(10.0, -7.0, -10.0);
    }
    glEnd();
}

