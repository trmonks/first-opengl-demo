# Setup

```
sudo apt-get install scons libglfw-dev
```

# Build

```
scons
```

# Run

```
./first
```

It faces you the wrong way - hold right to turn around
